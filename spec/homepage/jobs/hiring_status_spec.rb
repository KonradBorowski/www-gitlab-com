# frozen_string_literal: true

describe Gitlab::Homepage::Jobs::HiringStatus do
  describe '#under_sanction?' do
    it 'return true when sanction: true' do
      subject = described_class.new({ 'sanction' => true })

      expect(subject.under_sanction?).to be_truthy
    end

    it 'return false when sanction: false' do
      subject = described_class.new({ 'sanction' => false })

      expect(subject.under_sanction?).to be_falsey
    end
  end

  describe '#hiring?' do
    it 'return true when hiring: true' do
      subject = described_class.new({ 'hiring' => true })

      expect(subject.hiring?).to be_truthy
    end

    it 'return false when hiring: false' do
      subject = described_class.new({ 'hiring' => false })

      expect(subject.hiring?).to be_falsey
    end
  end

  describe '#hiring_limit' do
    context 'when not under sanction' do
      it 'return defined hiring limit when one is defined' do
        subject = described_class.new({ 'hiring_limit' => 10 })

        expect(subject.hiring_limit).to eq(10)
      end

      it 'return "no limit" when set to "no" or "false"' do
        subject = described_class.new({ 'hiring_limit' => false })

        expect(subject.hiring_limit).to eq('no limit')
      end
    end

    context 'when under sanction' do
      it 'return WIP' do
        subject = described_class.new({ 'hiring_limit' => 10, 'sanction' => true })

        expect(subject.hiring_limit).to eq('WIP')
      end
    end
  end

  describe '.no_hiring_list' do
    before do
      example_data = [
        { 'country' => 'Tropico', 'sanction' => true },
        { 'country' => 'SimNation', 'hiring_limit' => false },
        { 'country' => 'Azeroth', 'hiring' => false }
      ]

      allow(described_class).to receive(:raw_data).and_return(example_data)
    end

    it 'return countries under sanction' do
      countries = described_class.no_hiring_list.map { |hiring_status| hiring_status['country'] }

      expect(countries).to include('Tropico')
    end

    it 'return countries we cant hire' do
      countries = described_class.no_hiring_list.map { |hiring_status| hiring_status['country'] }

      expect(countries).to include('Azeroth')
    end
  end
end
