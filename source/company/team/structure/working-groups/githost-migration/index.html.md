---
layout: markdown_page
title: "GitHost Migration Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | January 30, 2020 |
| Target End Date | February 28, 2020|
| Slack           | [#wg_githost_migration](https://gitlab.slack.com/archives/CRKL886F2) (only accessible from within the company) |
| Google Doc      | [GitHost Migration Working Group Agenda](https://docs.google.com/document/d/1O8cF5ylQHJDAXVB3KUoW5FhPRH-RIOpHAOuuuPz0VL0/edit) (only accessible from within the company) |

## Business Goal

Successfully migrate remaining customers from GitHost.io to GitLab.com, allowing us to fully retire the GitHost service.

## Exit Criteria (-%)

1. Define acceptance criteria for each customer.
1. Identify and deliver issues blocking migration efforts in a [relevant epic](https://gitlab.com/groups/gitlab-org/-/epics/1952).
1. Complete an end-to-end testing plan and conduct a migration dry-run.
1. Document known risks as part of the migration process.
1. Execute on migration for all customers.

## Roles and Responsibilities

| Working Group Role    | Person                | Title                                      |
|-----------------------|-----------------------|--------------------------------------------|
| Executive Stakeholder | Christopher Lefelhocz | Senior Director of Development             |
| Facilitator           | Liam McAndrew         | Backend Engineering Manager, Manage:Import |
| Functional Lead       | Jeremy Watson         | Group Product Manager, Manage              |
| Member                | Haris Delalić         | Product Manager, Manage:Import             |
| Member                | George Koltsov        | Backend Engineer, Manage:Import            |
| Member                | Desiree Chevalier     | Software Engineer in Test, Manage          |
| Member                | Glen Miller           | Professional Services Engineer             |
| Member                | Petar Prokic          | Professional Services Engineer, EMEA       |
| Member                | Sean Hoyle            | Technical Account Manager                  |

## Timeline
