---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data as of 2019-12-31

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 81        | 7.42%           |
| Based in EMEA                               | 310       | 28.41%          |
| Based in LATAM                              | 14        | 1.28%           |
| Based in NORAM                              | 686       | 62.88%          |
| **Total Team Members**                      | **1,091** | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 777       | 71.22%          |
| Women                                       | 313       | 28.69%          |
| Other Gender Identities                     | 1         | 0.09%           |
| **Total Team Members**                      | **1,091** | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 51        | 77.27%          |
| Women in Leadership                         | 15        | 22.73%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **66**    | **100%**        |

| **Gender in Development**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Development                          | 378       | 82.71%          |
| Women in Development                        | 78        | 17.07%          |
| Other Gender Identities                     | 1         | 0.22%              |
| **Total Team Members**                      | **457**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.15%           |
| Asian                                       | 41        | 6.33%           |
| Black or African American                   | 22        | 3.40%           |
| Hispanic or Latino                          | 36        | 5.56%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 25        | 3.86%           |
| White                                       | 382       | 58.95%          |
| Unreported                                  | 141       | 21.76%          |
| **Total Team Members**                      | **648**   | **100%**        |

| **Race/Ethnicity in Development (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 18        | 9.78%           |
| Black or African American                   | 4         | 2.17%           |
| Hispanic or Latino                          | 10        | 5.43%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 7         | 3.80%           |
| White                                       | 113       | 61.41%          |
| Unreported                                  | 32        | 17.39%          |
| **Total Team Members**                      | **184**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 8         | 15.38%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 2         | 3.85%           |
| White                                       | 30        | 57.69%          |
| Unreported                                  | 12        | 23.08%          |
| **Total Team Members**                      | **52**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.09%           |
| Asian                                       | 96        | 8.80%           |
| Black or African American                   | 28        | 2.57%           |
| Hispanic or Latino                          | 53        | 4.86%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 33        | 3.02%           |
| White                                       | 600       | 55.00%          |
| Unreported                                  | 280       | 25.66%          |
| **Total Team Members**                      | **1,091** | **100%**        |

| **Race/Ethnicity in Development (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 48        | 10.50%          |
| Black or African American                   | 6         | 1.31%           |
| Hispanic or Latino                          | 24        | 5.25%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 13        | 2.84%           |
| White                                       | 254       | 55.58%          |
| Unreported                                  | 112       | 24.51%          |
| **Total Team Members**                      | **457**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 8         | 12.12%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 1.52%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 2         | 3.03%           |
| White                                       | 36        | 54.55%          |
| Unreported                                  | 19        | 28.79%          |
| **Total Team Members**                      | **66**    | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 18        | 1.65%           |
| 25-29                                       | 204       | 18.70%          |
| 30-34                                       | 306       | 28.05%          |
| 35-39                                       | 226       | 20.71%          |
| 40-49                                       | 223       | 20.44%          |
| 50-59                                       | 102       | 9.35%           |
| 60+                                         | 11        | 1.01%           |
| Unreported                                  | 1         | 0.09%           |
| **Total Team Members**                      | **1,091** | **100%**        |


Source: GitLab's HRIS, BambooHR
