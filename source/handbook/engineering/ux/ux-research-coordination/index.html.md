---
layout: handbook-page-toc
title: "UX Research Coordination"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Research Coordination at GitLab

Research Coordinators at GitLab are responsible for managing all aspects of participant recruitment for GitLab’s user experience research studies including, but not limited to, sourcing, outreach, screening, scheduling, participation agreements, and incentives management. 

To request support from a Research Coordinator, please follow the [process outlined here](/handbook/engineering/ux/ux-research/#how-ux-research-product-and-product-design-work-together-on-research).

### Recruitment methods

1. **GitLab First Look (formerly the UX Research Panel)** is a group of users who have opted in to receive research studies from GitLab. To find out more or to join, please visit [GitLab First Look](/community/gitlab-first-look/index.html). This is a good fit for studies that are aimed at GitLab users who are software developers and related roles. The panel currently does not have many security professionals, nor senior engineering leaders. 

1. **Respondent.io** is a recruitment service that is a good choice for studies aimed at software professionals who are not necessarily GitLab users. This has been a decent source of security professionals and some other harder-to-reach users. 

1. **Social outreach.** Social posts may go out on GitLab's brand channels. The Research Coordinator uses the Social Request template in the corporate marketing project to request this type of post. This is a good choice for studies primarily aimed at GitLab users. Product Managers, Product Designers, and other teammates are highly encouraged to help promote their studies to their networks.

### Thank you gifts

As a small token of thanks (and to ensure a higher participation/completion rate) we send gifts to research participants. These can be requested by opening an issue in the [UX research project](https://gitlab.com/gitlab-org/ux-research/blob/master/.gitlab/issue_templates/Incentives%20request.md).

* User interviews, usability testing, or 'buy a feature' research: $60 (or equivalent currency) Amazon gift card per 30 minutes.

* Surveys, beta testing, card sorts, and tree tests: Opportunity to win 1 of 3 $30 (or equivalent currency) Amazon Gift cards, or a GitLab swag item. 

* Design evaluations: Unpaid.

* Swag can also be sent on an ad hoc basis, as requested by researchers, PMs, and others. This is a nice touch and an opportunity for personalization after a particularly good conversation. Coordinators can reach out to the corporate events team (Emily Kyle) for swag codes.

#### Fulfillment

Requests for thank you gifts should be fulfilled at least twice per week so that users receive their gift promptly after participating in research. It's important to maintain a customer service mindset when interacting with participants - receiving their gift is the end of the research cycle and another touchpoint in their relationship with GitLab.

##### Tango Card

Tango Card can be used to send gift cards to users around the world. 

The accounting team funds the account with a lump sum amount from the pre-approved research incentives budget. We issue cards from that prepaid amount. The research coordinator sends a report by the 1st of every month to `ap@gitlab.com` with the following information:

* How much the account was funded at the beginning of the month
* How much was spent on issued cards
* The final balance in the account

The report should be for the previous month (e.g., the report should be sent by 11/1 for cards send between 10/1 and 10/31).

##### Respondent.io

Respondent requires us to pay participants directly through the platform. Coordinators should use their personal card and submit prompt expense reports through Expensify for reimbursement. 

##### Directly through Amazon

**This should not be necessary while Tango Card is operational.** Amazon gift cards are country specific. When purchasing a gift card, ensure you use the appropriate Amazon store for a user's preferred country.

When attempting to send incentives internationally, your credit card or PayPal may be denied. You can offer to send an Amazon.com card (not in the user's country's store) or use [transferwise](https://transferwise.com/), which requires bank routing information. 

### Email tips

When sending emails to GitLab First Look, coordinators may customize emails in a few ways. 
* Always send yourself at least one test email
* Add an opening line above the study details. This can be a way to reference the time of year or add other personalization not in the template.  
* Feel free to lightly re-write parts of the email template to make them sound more like your voice. Some First Look participants receive many emails from us, so changing them up slightly will be nice reinforcement that a human is actually on the other side of the process.
* Experiment with subject lines (`We have a new study for you!`; `See if you're a match with our new study`; etc.)
* Adjust the `sent from` display name to `Name from Gitlab`
* If your response rate is low, but you think that your target participants are in your segment, send one reminder email rather than immediately starting a new segment.
    - Send the reminder no sooner than 4 days after your first email was sent 
    - Add a brief note to the top of the email. Otherwise Qualtrics will just send a link to the survey
    - Write a new subject line (`We still want to hear from you!`; `Reminder: Take our quick survey`)
    - If you suspect that not many of your target participants are in your segment (i.e., because they are security professionals or others that we've struggled to recruit) don't send a reminder. Sending too many reminders increases the risk of unsubscribes. 

### UX research promotion

Coordinators should set aside time to interact with users on social. It's recommended to post any new UX blog posts and search `gitlab ux` at least once per week. 

When appropriate, link to GitLab First Look (and [grab an image](https://gitlab.com/gitlab-com/marketing/corporate-marketing/tree/master/design/social-media/ads-share-images/gitlab-first-look/png) to upload) in responses to comments about GitLab's UX. This is important for raising awareness of the research program, growing the panel, and performing research with users who may have had negative experiences with GitLab. 

Some sample responses:
* (To a positive comment) `Thank you so much! Would you consider joining our research program if you haven't already? You can specify which areas of the product you're interested in: https://about.gitlab.com/community/gitlab-first-look/`
* (To a negative comment) `I'm so sorry to hear you've had a negative experience! Would you consider giving feedback through our research program? You can specify exactly which parts of the product you're interested in: https://about.gitlab.com/community/gitlab-first-look/`

If possible, also tweet (and/or blog!) about interesting findings that arise from studies in a timely fashion. The best content comes from chronicling feedback we received and what we did with it. It's important to close the feedback loop and demonstrate that we're not only listening, but taking action. 

## Recruitment case study

This is a supplement to the [recruitment training materials](/handbook/engineering/ux/ux-research-training/#how-to-recruit-participants). 

### Accessibility solution validation study 

[Research issue](https://gitlab.com/gitlab-org/ux-research/issues/576)

#### Identify target respondents

The stated goal of the study:

> As part of our continued effort to improve the automated testing capabilities of GitLab, we want to ship a new feature for accessibility testing. We want to validate some solutions for this feature…questions we have are:
> - How exactly users want to see Accessibility issues introduced during their development shown to them
> - Which of the proposed designs suffice their use case for Automated Accessibility Testing as part of the CI process

**How many participants do they need?** 
It’s a usability test, so around five users is generally the sweet spot.

**What's the timeline?** 
The designer stated that they're ready to begin any time, and notified the coordinator of their ooo time. 

**Who do they want to speak with?** 
The criteria that was submitted was the following:

> Frontend engineers who are working to ensure their code is accessibility compliant

There's some ambiguity in the phrasing here that we need to clear up before sending out a screener. Does this mean we want people whose job is to test for accessibility compliance? Unfortunately, these people are rare and historically difficult to find for studies. Often, accessibility is a subject that people are passionate about, and champion unofficially rather than as a stated responsibility of their role. This is a case where a job title or job responsibilities would not be good criteria to strictly adhere to. 

In our recruiting issue, the coordinator worked with the designer to clarify the criteria. Both agreed that we could also invite people to interview who did not say that they were responsible for accessibility compliance in their role, but who strongly agreed that accessibility compliance is important.

#### Craft your screener

In our example usability test, the key questions that recruiting will hinge on are really clear - the coordinator expects to see a question about job title, common tasks you perform or responsibilities users have, and something about how users perceive the importance of accessibility compliance. The coordinator also expects to see things that we almost always include - things like company size and industry, which we try to get a mixture of when they’re not the object of the study. 

This study is unusual in that we're not asking about what tools people use. Often, we explicitly want to see what someone with or without GitLab experience thinks. It's not relevant here, so we're not collecting that information. Keeping things short and sweet increases the completion rate of the screener. 

**Check out the [finished screener](https://gitlab.eu.qualtrics.com/jfe/preview/SV_cT80heuzGIlAdZH?Q_SurveyVersionID=current&Q_CHL=preview) for our usability test.**

#### Open a recruitment request

A lot of the conversation above occurred in the recruitment request; sometimes these phases occur concurrently. When the coordinator and designer are clear about the criteria, and the screener is added to Qualtrics with the help of the researcher, the invitations are ready to go out. 

The coordinator takes care of creating a segment from our database. Because it's about accessibility, a topic about which many people are passionate, this study is a good one to use custom subject lines and email copy. The coordinator sent out an email with subject line `New study - let's talk accessibility`. After two emails, we received over 20 responses. 

The coordinator highlighted in green the people who said they do accessibility testing in their jobs, and strongly agreed that accessibility compliance is important (4 total). They highlighted in orange people who only fit the latter criterion (9). Altogether, this is a surplus of participants for the current study, and we have some to reach out to directly for the next available accessibility study. 

**See how the [recruitment process](https://gitlab.com/gitlab-org/ux-research/issues/577), scheduling, and thank you gifts were coordinated for our example study.**








