---
layout: handbook-page-toc
title: Certifications
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Welcome to the certifications page! Here you will find links to our current certifications.

## Current Certifications 

* [GitLab Values](/handbook/values/#gitlab-values-certification)

## Upcoming

In FY21-Q1 we plan to roll out the certifications listed below. 

* GitLab Communication
* Inclusion
* Being an Ally
